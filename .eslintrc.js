module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 8,
    sourceType: 'module'
  },
  extends: ['prettier', 'plugin:prettier/recommended'],
  plugins: ['prettier', 'json'],
  // add your custom rules here
  rules: {
    'no-console': 0
  }
}
