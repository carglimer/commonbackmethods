// const fs = require('fs-extra')

module.exports.getLinkId = (link) => {
  let linkId
  if (!link) {
    return linkId
  }
  // ჯერ 3 ვარიანტი გვაქვს ebay-kleinanzeigen, mobile.de, autoscout24.de
  const patternEbayKleinanzeigen = /http[s]?.*?ebay-kleinanzeigen.*?(\d{10})/ // https://www.ebay-kleinanzeigen.de/s-anzeige/skoda-fabia-combi-ambition-ahk-tempomat-euro-6/1843952337-216-2452
  const patternMobile = /http[s]?.*?mobile.de.*?id=(\d{9})/ // https://suchen.mobile.de/fahrzeuge/details.html?id=328942887&damageUnrepaired=ALSO_DAMAGE_UNREPAIRED&isSearchRequest=true&pageNumber=1&scopeId=C&sfmr=false&searchId=742b336d-ef15-5819-a108-283ce6e8f36c
  const patternMobile2 = /http[s]?.*?mobile.de.*?(\d{9})/ // https://suchen.mobile.de/auto-inserat/volkswagen-polo/333164230.html?coId=f881b725-39fc-45ab-9070-eb063030fbef&action=vip404SingleAdReco
  const patternAutoscout = /http[s]?.*?autoscout24.*?searchId=(\d{10})/ // https://www.autoscout24.de/angebote/ford-transit-kombi-ft-300-l-1-hand-klima-rollstuhl-diesel-weiss-7eedf472-dec1-48c8-b147-6f4afb930624?&cldtidx=7&cldtsrc=listPage&searchId=1239112503
  if (link.match('ebay-kleinanzeigen')) {
    const res = patternEbayKleinanzeigen.exec(link)
    if (res && res[1]) {
      linkId = res[1]
    }
  } else if (link.match('mobile.de')) {
    let res = patternMobile.exec(link)
    if (res && res[1]) {
      linkId = res[1]
    }
    if (!linkId) {
      res = patternMobile2.exec(link)
      if (res && res[1]) {
        linkId = res[1]
      }
    }
  } else if (link.match('autoscout24')) {
    const res = patternAutoscout.exec(link)
    if (res && res[1]) {
      linkId = res[1]
    }
  }
  return linkId
}
